<?php include 'vars.php' ?>
<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Literary Editing and Writing Services by Stephen Gashler | GetGash.com</title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="/fonts/my-icons-collection/font/flaticon.css" rel="stylesheet">
        <link href="/theme.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>
        <header>
            <div id="menu-1">
                <div class="content">
                    <div class="logo"><i class="flaticon-dragon"></i> GetGash | <span class="tagline">Literary Services</span></div>
                    <div class="phone"><?php echo $vars['phone'] ?></div>
                </div>
            </div>
            <nav id="menu-2">
                <div class="icons">
                    <a href="#fiction">
                        <i class="flaticon-saturn"></i>
                        <div>Fiction</div>
                    </a>
                    <a href="#non-fiction">
                        <i class="flaticon-earth"></i>
                        <div>Non-fiction</div>
                    </a>
                    <a href="#children">
                        <i class="flaticon-children"></i>
                        <div>Children's</div>
                    </a>
                    <a href="#non-fiction">
                        <i class="flaticon-chart"></i>
                        <div>Business</div>
                    </a>
                </div>
            </nav>
            <div id="menu-3">
                <div class="menu-button pull-right">
                    <i class="fa fa-bars" onclick="toggleMenu('#menu-3 nav')"></i>
                </div>
                <nav>
                    <li><a href="#services">Editing</a></li>
                    <li><a href="#services">Proofreading</a></li>
                    <li><a href="#services">Copywriting</a></li>
                    <li><a href="#services">Content Editing</a></li>
                    <li><a href="#services">Ghostwriting</a></li>
                </nav>
            </div>
        </header>
        <div class="clear"></div>
        <article>
            <section id="home">
                <h1>
                    Literary Editing and Writing Services by Award-winning Author Stephen Gashler
                </h1>
                <img
                    id="portrait"
                    class="pull-left thumbnail"
                    src="/img/stephen-gashler.jpg" alt="Stephen Gashler"
                    style="max-width: 200px;"
                >
                <p>
                    <span class="highlight">Do you have an idea, draft, or manuscript?</span> No matter what stage of the writing process you're in, chances are you need an experienced professional to help with grammar, style, content, and effect. Especially in the competetive literary industry, it takes an informed opinion and a fresh pair of eyes to move an idea from good to great. I'll help you plan, critique, rewrite, polish, and even pitch your manuscript to agents or publishers that best suit you.
                </p>
                <p>
                    <span class="highlight">Hi, I'm Steve</span>. First and foremost, I am a writer and storyteller. I've developed and workshoped many manuscripts. I've published several novels, both through publishing companies and through self-publishing. I've worked with countless authors at writing symposiums and spoken on many writing panels. I've worked as an independent author and as a copywriter on marketing teams. I've told stories throughout the United States and have taken home a first place award at a national contest and five audience choice awards.
                </p>
                <p>
                    <span class="highlight">Most likely you've spent countless hours creating your baby</span>. Don't let all that hard work end up in the rejection pile in a publisher's office. Give me a call or send me an email. We'll talk about your project, your goals, and how I can help you achieve literary success.
                </p>
                <div class="quote">
                    <p>
                        "Stephen Gashler has a good sense of story. He has the ability to choose material that fits his audience."
                    </p>
                    <cite>
                        Carla Morris - Children’s Services Manager<br>
                        Provo City Library
                    </cite>
                </div>
                <div class="align-center">
                    <a class="btn" href="#contact">Contact Me</a>
                </div>
            </section>
            <section id="fiction">
                <h2><i class="flaticon-saturn"></i> Fiction</h2>
                <p>
                    <span class="highlight">I'm passionate about fiction</span>. My specialties are science fiction and fantasy, but I enjoy most genres. If it's imaginative and inspires readers to think beyond the every day, then I'd love to see what you've got. Not quite there yet? That's why I'm here, so I'd love to see it anyway! Depending on your needs and stage of the writing process, I'll help you outline your plot, strengthen your characters, expand your world, clean up your manuscript, and give honest feedback as a reader. I'll also help you prepare a pitch for an agent or publisher. If your work is still in the early stages or you're not sure about the wordcount, no problem. <a href="#contact">Give me a call or send me an email</a>, and we'll work out a quote.
                </p>
                <div class="quote">
                    <p>"Stephen Gashler has a gift for capturing the imagination of audiences of all ages"</p>
                    <cite>Rich Thurman – Director<br>Utah Valley Renaissance Faire</cite>
                </div>
                <div class="quote">
                    <p>"Stephen Gashler has excellent comedic timing"</p>
                    <cite>Larisa Hicken<br>Front Row Reviewers Utah</cite>
                </div>
                <div class="quote">
                    <p>"Stephen Gashler shows all the promise of a truly great artist"</p>
                    <cite>Aubrey Warner<br>Utah Theatre Bloggers</cite>
                </div>
            </section>
            <section id="non-fiction">
                <h2><i class="flaticon-earth"></i> Non-fiction, Business, and Marketing</h2>
                <p>
                    <span class="highlight">I've spent years working on marketing teams</span>, researching, writing, and editing white papers, blog articles, script content, and ad copy. Whether your goal is publication, SEO, or lead-generation, I can help you by researching your topic, editing your work, or writing original content. <a href="#contact">Give me a call or send me an email</a>, and let's talk about your project.
                </p>
            </section>
            <section id="children">
                <h2><i class="flaticon-children"></i> Children's Literature</h2>
                <p>
                    <span class="highlight">My first love was with children's literature</span>. I got my start in writing scripts for young audiences, and I've performed at countless libraries and schools as a children's entertainer. Together, my wife and I have developed manuscripts for picture books, juvenile, middle grade, and young adult novels. Let me help you prepare your idea or manuscript for your target demographic and share the insights I've gained about the industry. <a href="#contact">Give me a call or send me an email</a>, and let's talk about your project.
                </p>
            </section>
            <section id="services">
                <h2>Services and Rates</h2>
                <ul>
                    <li>
                        <span class="highlight">Proofreading</span><br>
                        Fixing grammar and formatting errors - <?php echo $vars['rates']['hour'] ?>.
                    </li>
                    <li>
                        <span class="highlight">Copy Editing</span><br>
                        Reworking sentences to ensure clear, correct, concise, complete, and consistent prose - <?php echo $vars['rates']['hour'] ?>.
                    </li>
                    <li>
                        <span class="highlight">Content Editing</span><br>
                        Deep analysis, critiquing, and (if desired) working with you to rewrite substantial portions of your work - <?php echo $vars['rates']['thousand'] ?>.
                    </li>
                    <li>
                        <span class="highlight">Writing</span><br>
                        Researching a topic and writing about it for your publication as a credited author - <?php echo $vars['rates']['word_credited'] ?>.
                    </li>
                    <li>
                        <span class="highlight">Ghostwriting</span><br>
                        Researching a topic and writing about it for your publication under your name or company name - <?php echo $vars['rates']['word_ghostwriter'] ?>.
                    </li>
                </ul>
            </section>
            <section id="contact">
                <h2>Contact Me</h2>
                <p>Questions? Ready to get started? Call me any time. Seriously! If I don't answer, leave a message, and I'll get back to you as soon as possible.</p>
                <i class="fa fa-phone"></i> <?php echo $vars['phone'] ?><br>
                <i class="fa fa-envelope"></i> <?php echo $vars['email'] ?>
            </section>
            <footer>
                Copyright <?php echo date('Y') ?> &copy; Stephen Gashler<br>
                AngelFox Media LLC
            </footer>
        </article>
        <script>
            function toggleMenu(menu)
            {
                $(menu).slideToggle();
            }

            function scrollTo(element)
            {
                $('html, body').animate({
                    scrollTop: $(element).offset().top
                }, 2000);
            }
        </script>

        <!-- Google Code for Remarketing Tag -->
        <!--------------------------------------------------
        Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
        --------------------------------------------------->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 844894476;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/844894476/?guid=ON&amp;script=0"/>
        </div>
        </noscript>
        <!-- Adwords phone tracking -->
        <script type="text/javascript">
            (function(a,e,c,f,g,h,b,d){var k={ak:"844894476",cl:"YIMICMrRpnMQjKLwkgM",autoreplace:"(385) 482-0007"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
        </script>
    </body>
</html>
